import { Component, OnInit  } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app';
  movies: string[];
  newName: string;

  constructor(private _httpService: Http){

  }

  ngOnInit(){
    this.reloadMoviesList();
  }

  deleteMovie(index){
    var movieToRemoveName = this.movies[index];

    this._httpService.delete('/api/movies?movieName=' + movieToRemoveName).subscribe(res=>{
        this.reloadMoviesList();
    });
  }

  addMovie(){
    var movie = {
        title: this.newName
    }

    this._httpService.post('/api/movies', movie).subscribe(res=>{
        this.reloadMoviesList();
        this.newName = '';
    });
  }

  private reloadMoviesList(){
  this._httpService.get('/api/movies').subscribe(values => {
         this.movies = values.json() as string[];
      });
  }
}
