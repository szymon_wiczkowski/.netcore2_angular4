﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Future3_v1.Controllers
{
    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        static List<string> movies = new List<string>() {
            "Matrix",
            "Star Wars - New Hope",
            "Star Wars - Empire Strikes Back",
            "Star Wars - Return Of The Jedi",
            "Batman",
            "Batman Returns",
            "Dark Knight",
        };

        public MoviesController()
        {
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return movies;
        }

        [HttpPost]
        public void Insert([FromBody] Movie newMovie)
        {
            movies.Add(newMovie.Title);
        }

        [HttpDelete]
        public void Delete(string movieName)
        {
            movies.Remove(movieName);
        }
    }

    public class Movie
    {
        public string Title { get; set; }
    }
}